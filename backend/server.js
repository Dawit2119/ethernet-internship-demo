import { VerifyCurrentToken } from "./Middleware/AuthMiddleware.js";
import {databaseconfiguration} from './Config/DatabaseConfig.js'
import express from 'express'
import allroutes from './Routes/AllRoutes.js'
import cors from 'cors'
databaseconfiguration();
const app = express(); 
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())


const PORT = process.env.PORT || 4000;

app.use("/api", allroutes);

app.get("/", VerifyCurrentToken, (httprequest, httpresponse) => {
  httpresponse.json({
    message: "Hey All",
  });
});

const server = app.listen(PORT, () => {
  console.log(`Server is running at ${PORT}`);
});