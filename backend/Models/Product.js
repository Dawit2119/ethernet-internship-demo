import mongoose from 'mongoose'
const Schema = mongoose.Schema

const ProductSchema = new Schema({
    productName:{
        type:String,
        required: true,
        
    }, 
    category:{
        type:String,
        required:true
    },
    date:{
        type:String, 
        required:true, 
        default:new Date()
    }, 
    freshness:{
        type:String, 
        required:true, 
    }, 
    price:{
        type:Number, 
        required:true
    }, 
    comment:{
        type:String
    }
})
export default mongoose.model("Product",ProductSchema)