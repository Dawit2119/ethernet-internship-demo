import Product from "../Models/Product.js";

export const AddProduct = async(req,res)=>{
  const request = req.body
  console.log("Add product request is comming");
  console.log(request);
  try {
      const product = await Product.create(request)
      return res.status(201).json({product})
  } catch (error) {
      console.log(error.message);
     res.status(500).json({message:error.message})
  }
}
export const getAllProducts = async(req,res)=>{
  try {
      const products = await Product.find()
      return res.status(200).json({products})
  } catch (error) {
      return res.status(500).json({message:error.message})
  }
}
export const updateProduct = async(req,res)=>{
    try {
        const id = req.params.id 
        console.log("id");
        console.log(id);
        console.log("update request is comming");
        const updateInfo = req.body
        const product = await Product.findById(id)
        if(product){
            await Product.updateOne(product, {$set:updateInfo})
            return res.status(201).json({message:"update success"})
        }
    } catch (error) {
        return res.status(500).json({message:error.message})
    }

} 
export const deleteProduct = async(req,res)=>{
    try {
        const id = req.params.id 
        const product = await Product.findById(id)
        if(product){
            await Product.deleteOne(product)
            return res.status(201).json({message:"Delete  success"})
        }
    } catch (error) {
        return res.status(500).json({message:error.message})
    }
}