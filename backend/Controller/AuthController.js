import jwt from 'jsonwebtoken'
import bcryptjs from 'bcryptjs'
import UserModel from '../Models/User.js';
import RepresentativeModel from '../Models/Representative.js';
import AppUserModel from '../Models/AppUser.js';
import dotenv from 'dotenv'
dotenv.config()

// Signup Controller 

export  const Signup = async (req, res)=>{
  console.log("register request is comming")
    console.log(req.body)
    try {

        const fullName = req.body.fullName;

        const userName = req.body.userName;

        const email = req.body.emailAddress;

        const password = req.body.password;

        const confirmPassword = req.body.confirmPassword;

        const userRole = req.body.userRole
        
        if (!(email && password && fullName && userName)) {
            res.status(400).send("All input are required");
        }
        if(password !== confirmPassword){
          return res.status(409).json({message:"Passwords not match"})
        }
        const check_user = await  UserModel.findOne({ emailAddress:email });
      
        if (check_user) {
            return res.status(403).json("user already exist!");
            
        }
       
        const  encryptedPassword = await bcryptjs.hash(password, 10);
       
        const  encryptedConfirmPassword = await bcryptjs.hash(confirmPassword, 10);
        const userInfo = {
          
            fullName,

            userName,

            emailAddress: email.toLowerCase(), 

            password: encryptedPassword,
          
            confirmPassword:encryptedConfirmPassword,
            
            userRole

        }

        if (userRole === "Representative"){
          const newUser = await RepresentativeModel.create(userInfo)
          
          console.log("Succesfully Registered!");
          return  res.status(201).json(newUser);

        }
        if (userRole === "User"){
          const newUser = await AppUserModel.create(userInfo)

          console.log("Succesfully Registered!");
          return  res.status(201).json(newUser);
          
        }


        

       
    } catch (error) {
        console.log(`error: ${error}`);
        
    }



}




 

// Login Controller

export  const Login = async (req, res)=>{
  console.log("login request is comming")
  console.log(req.body);
  
  try{
      const {emailAddress , password} = req.body;

      if (!(emailAddress && password)) {

          res.status(400).send("All input is required");

        }
  

      const currentUser = await UserModel.findOne({emailAddress});
      const check_password = await bcryptjs. compare(password , currentUser.password);
      if (currentUser && check_password){

          const Generate_Token = jwt.sign(
              {currentUser}, 
              process.env.JWT_KEY || "My Secret Key", 
              {
                  expiresIn:"2h"
              }
          );
          res.status(200).json({"access_token":Generate_Token , "currentuser":currentUser});
      }
      else{

      return res.status(400).send("email or password not correct");
      }

  }catch(e){
      return res.status(403).send("email or password incorrect");
  }


}


export const getallusers = async (httpreq, httpres) => {
    try {
      const users = await UserModel.find();
      if (users != null) {
        return httpres.status(200).json({ Users : users });
      } else {
        return httpres.status(400).send("Sorry, didnt get any user!");
      }
    } catch (error) {
      console.log(error);
    }
  };
  