import { Signup, Login , getallusers } from "../Controller/AuthController.js";
import express from "express";
import { AddProduct,getAllProducts,updateProduct,deleteProduct } from "../Controller/ProductController.js";
// Auth Router

const allroutes = express.Router() ;

allroutes.post("/register", Signup);

allroutes.post("/login", Login);
//product
allroutes.post('/addproduct',AddProduct)
allroutes.get('/getproducts',getAllProducts)
allroutes.put('/updateproduct/:id',updateProduct)
allroutes.delete('/deleteproduct/:id',deleteProduct)
export default allroutes;

