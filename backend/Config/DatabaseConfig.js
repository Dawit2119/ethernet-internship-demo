
import mongoose from 'mongoose'

const databaseUrl = "mongodb://localhost/ethernetDemo";

export const databaseconfiguration = () =>{ 

    mongoose.connect(databaseUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      })
    .then(()=>{
        console.log("Database connected succesfully!");
    })
    .catch((error)=>{
        console.log(error);
    })

}


