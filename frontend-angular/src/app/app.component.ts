import { Component, Inject, OnInit,ViewChild } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from './components/dialog/dialog.component';
import { ApiService } from './services/product.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(public dialog:MatDialog,
    private apiService:ApiService,public authService:AuthService,
    private router:Router
    ){}
  title = 'angularmaterialdemo';
  displayedColumns: string[] = ['productName', 'category', 'date','freshness','price','comment','actions'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  openDialog() {
    this.dialog.open(DialogComponent, {
      width:'50%'
    }).afterClosed().subscribe(value=>{
      console.log(`value is ${value}`);
      if(value === "save"){
        this.getAllProducts()
      }
    })
  }
  editProduct(row:any){
    this.dialog.open(DialogComponent,{
      width:"50%",
      data:row
    }).afterClosed().subscribe(value=>{
      console.log(`value is ${value}`);
      if(value === "update"){
        this.getAllProducts()
      }
    })
  }
  isRepresentative():boolean{
    return localStorage.getItem("role") !== 'User';
  }
  logout(){
    this.authService.logout()
    this.router.navigate(['/'])
  }
  deleteProduct(id:number){
    this.apiService.deleteProduct(id).subscribe({
      next:(value)=>{
        alert("Product deleted successfully")
        this.getAllProducts()
      },
      error:()=>{console.log("Error while deleting a Product")}
    })
  }
  ngOnInit(): void {

    this.getAllProducts()
  }
  getLoggedUser(){
    return localStorage.getItem("email")
  }
  getAllProducts(){
    console.log("get all products is called");

    this.apiService.getProduct().subscribe({
      next:(products)=>{
        console.log("products");
        console.log(products.products);
        this.dataSource = new MatTableDataSource(products.products);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error:()=>{
        console.log("Error while fetching products")
      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
