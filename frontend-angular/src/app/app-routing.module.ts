import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { ErrorComponent } from './components/error/error.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { RepresentativehomeComponent } from './components/representativehome/representativehome.component';
import { UserhomeComponent } from './components/userhome/userhome.component';

const routes: Routes = [

  {path:"",component:AppComponent},
  {path:"login",component:LoginComponent},
  {path:"register",component:RegisterComponent},
  {path:"dialog",component:DialogComponent},
  {path:"user",component:UserhomeComponent},
  {path:"representative",component:RepresentativehomeComponent},
  {path:"**",component:ErrorComponent,pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
