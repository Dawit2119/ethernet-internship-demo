export interface User{
  id?:string,
  fullName:string;
  emailAddress:string;
  password:string;
  userName:string;
  confirmPassword:string;
  userRole:string;
}
