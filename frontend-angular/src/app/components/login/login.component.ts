import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginUserData = {emailAddress: '', password: ''}
  constructor(private _auth: AuthService, private router:Router) {}
  ngOnInit(): void {
  }
  loginUser(){
    this._auth.login(this.loginUserData)
    .subscribe({
      next:(value)=> {
        alert("login success")
        localStorage.setItem('token', value.access_token)
        var email = value.currentuser.emailAddress
        localStorage.setItem('email',email)
        // if(role == 'User'){
        //     this.router.navigate(["/user"])
        // }else{
          this.router.navigate(['/representative'])

      },
      error:(e)=>{
        console.log(e);

        alert(e.error)
      }


  })
}

}
