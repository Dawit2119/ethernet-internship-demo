import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepresentativehomeComponent } from './representativehome.component';

describe('RepresentativehomeComponent', () => {
  let component: RepresentativehomeComponent;
  let fixture: ComponentFixture<RepresentativehomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepresentativehomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepresentativehomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
