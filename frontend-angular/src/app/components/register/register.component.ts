import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from 'src/app/services/auth.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

  constructor(private _auth: AuthService, private _router: Router) {}

  user = {
    userName: '',
    emailAddress: '',
    password: '',
    fullName: '',
    confirmPassword: '',
    userRole: '',
  }
  // companyData = {companyAddress:'',companyName :""}
  ngOnInit() {}
  verifyPassword(): boolean {
    return this.user.password == this.user.confirmPassword
  }
  registerEmployee() {
    this.user.userRole = 'User'
    this._auth.register(this.user).subscribe({
      next: (value) => {
        console.log(value)
        alert('Register Success')
        this._router.navigate(['/login'])
      },
      error: (e) => {
        if (e.status != 409) {
          alert(e.error)
        }
      },
    })
  }
  registerEmployer() {
    this.user.userRole = 'Representative'
    this._auth.register(this.user).subscribe({
      next: (value) => {
        console.log(value)
        alert('Register Success')
        this._router.navigate(['/login'])
      },
      error: (e) => {
        if (e.status != 409) {
          alert(e.error)
        }
      },
    })
  }
}
