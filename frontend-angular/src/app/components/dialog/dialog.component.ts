import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/product.service';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  freshnessList = ['Fresh','Second Hand','reFurbished']
  productForm !: FormGroup;
  actionText:string = 'Save'
  constructor(private fomBuilder:FormBuilder,
    @Inject (MAT_DIALOG_DATA) public editData:any,
    private apiService:ApiService, private dialogRef:MatDialogRef<DialogComponent>) {
   }
  ngOnInit(): void {
    this.productForm = this.fomBuilder.group({
      productName: ['',Validators.required],
      category: ['',Validators.required],
      freshness: ['',Validators.required],
      price: ['',Validators.required],
      comment: ['',Validators.required],
      date: ['',Validators.required],
    })
    if(this.editData){
      this.actionText = 'Update'
      this.productForm.controls['productName'].setValue(this.editData.productName);
      this.productForm.controls['category'].setValue(this.editData.category);
      this.productForm.controls['freshness'].setValue(this.editData.freshness);
      this.productForm.controls['price'].setValue(this.editData.price);
      this.productForm.controls['date'].setValue(this.editData.date);
      this.productForm.controls['comment'].setValue(this.editData.comment);
    }
  }
  productUpdateAndCreate(){
    if(this.editData){
      this.apiService.editProduct(this.productForm.value,this.editData._id).subscribe({
        next:(res)=>{
          alert("product Updated successfully")
          this.productForm.reset()
          this.dialogRef.close('update');

        },
        error:()=>{
          alert("Error while Updating a product");
        }
      })

    }
    else{
    if(this.productForm.valid){
      this.apiService.postProduct(this.productForm.value).subscribe({
        next:(res)=>{
          alert("product Added successfully")
          this.productForm.reset()
          this.dialogRef.close('save');

        },
        error:()=>{
          alert("Error while adding a product");
        }
      })
    }
  }
  }



}
