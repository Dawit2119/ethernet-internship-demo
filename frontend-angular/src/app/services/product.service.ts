import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
const options = new HttpHeaders({
  'Content-Type':'applicaton/json'
})
@Injectable({
  providedIn: 'root'
})
export class ApiService {
   apiUrl = "http://localhost:4000/api"
  constructor(private http:HttpClient) {

   }
   postProduct(data:any){
     console.log("data is ");
     console.log(data);
     var response =  this.http.post<any>(`${this.apiUrl}/addproduct`,data);
     console.log("response");
     console.log(response);
     return response;


   }
   editProduct(data:any,id:number){
     console.log(data);
    return this.http.put<any>(`${this.apiUrl}/updateproduct/${id}`,data);
  }
   getProduct(){
     return this.http.get<any>(`${this.apiUrl}/getproducts`)
   }
   deleteProduct(id:number){
     return this.http.delete<any>(`${this.apiUrl}/deleteproduct/${id}`)
   }
}
