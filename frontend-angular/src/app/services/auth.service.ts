import { Injectable } from '@angular/core';
import { User } from '../models/User';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const options = new HttpHeaders({
  'Content-Type':'application/json'
})
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiURL = 'http://localhost:4000/api';
  constructor(private http:HttpClient) { }
  register(user:any):Observable<any>{
    var response =  this.http.post<any>(`${this.apiURL}/register`,user)
    console.log("response");
    console.log(response);
    return response;


    // return this.http.post<any>(`${this.apiURL}/register`,user);
  }
  login(user:any):Observable<any>{
     return this.http.post<any>(`${this.apiURL}/login`,user)
  }
  isLoggedin():boolean{
    return localStorage.getItem('token') != undefined;
  }
  logout(){
    localStorage.removeItem('token')
  }
}
